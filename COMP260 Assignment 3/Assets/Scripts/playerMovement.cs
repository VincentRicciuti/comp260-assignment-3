﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class playerMovement: MonoBehaviour
{

	private Rigidbody playerRigidbody;
	public float speed = 50.0f;
	Vector3 targetPos = Vector3.zero;

	void Start ()
	{
		playerRigidbody = GetComponent<Rigidbody> ();
		playerRigidbody.useGravity = false;
	}

	void Update ()
	{
		float step = speed * Time.fixedDeltaTime;

		if (Input.GetMouseButtonDown (1)) {
			targetPos = GetMousePos ();
		}
		playerRigidbody.position = Vector3.MoveTowards (playerRigidbody.position, targetPos, step);
	}

	private Vector3 GetMousePos ()
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.red;
		Vector3 pos = GetMousePos ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
}
