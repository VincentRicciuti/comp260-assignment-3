﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMove : MonoBehaviour {

	public float speed = 4.0f;
	public Transform target;
	private Vector2 direction;
	// Use this for initialization
	void Start() {
		playerMovement player = FindObjectOfType<playerMovement>();
		target = player.transform;

		direction = target.position - transform.position;
		direction = direction.normalized;
	}

	// Update is called once per frame
	void Update () {
		Vector2 velocity = direction * speed;
		transform.Translate (velocity * Time.deltaTime);
	}
}
