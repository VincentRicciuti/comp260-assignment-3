﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawner : MonoBehaviour {
	public enemyMove enemyPrefab;
	public playerMovement player;
	public int numOfEnemies = 1;
	public float minX, minY, width, height;
		
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		GameObject[] aliveEnemies;
		aliveEnemies = GameObject.FindGameObjectsWithTag ("EnemyTag");
		if (aliveEnemies.Length == 0) {
			numOfEnemies++;
			spawnEnemies();
		}


	}

	float[] createPos()
	{
		float x = minX + Random.value * width;
		float y = minY + Random.value * height; 

		float[] returnArr = new float[2];

		returnArr [0] = x;
		returnArr [1] = y;

		return returnArr;
	}

	void spawnEnemies()
	{
		for (int i = 0; i < numOfEnemies; i++) {
			enemyMove enemy = Instantiate (enemyPrefab);

			float x = createPos()[0];
			float y = createPos()[1];

			enemy.transform.position = new Vector2 (x, y);
		}
	}
}
